/*
 * filter/linktty.c - forward data between two raw-mode pty devices, e.g.
 * for use as a gdb 'remote' debugging channel which doesn't accept
 * just any old connection from the network
 */

#include <assert.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ioctl.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/mount.h>

#include "filter.h"

char *translate(tstate *state, char *data, int inlen, int *outlen,
		double *delay, int input, int flags)
{
    return NULL;
}

void errmsg(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "linktty: ");
    vfprintf(stderr, fmt, ap);
    fputc('\n', stderr);
    va_end(ap);
}

static struct pty {
    int master, slave;
    const char *linkfile;
} ptys[2];

static void help(FILE *fp)
{
    fputs("\
usage: linktty [PATH1 [PATH2 [COMMAND ARGS...]]]\n\
where: PATH1           symlink tty #1 to here (else just print the tty name)\n\
       PATH2           symlink tty #2 to here (else just print the tty name)\n\
       COMMAND ARGS    run this command and terminate when it does\n\
also:  linktty --help  display this text\n\
", fp);
}

int main(int argc, char **argv)
{
    int nlinkfiles = 0;
    bool doing_opts = true;
    char **command = NULL;
    int i;

    /*
     * Process command line.
     *
     * Usage:
     *
     * Just 'linktty' will allocate two ptys and print their names on
     * standard output, then wait for ever forwarding data back and
     * forth until killed.
     *
     * Adding one or two arguments will cause linktty to symlink the
     * ptys to those locations, so that the programs that will be
     * using them can find them at predictable locations. The program
     * will still forward until killed.
     *
     * Any further arguments will be treated as a command to be
     * executed in a child process, and when that child terminates, so
     * will linktty. So you could, for example, execute
     *
     *    linktty link1 link2 gdbserver link2 <command to debug>
     *
     * and then, in another terminal, gdb -ex 'target remote link1' to
     * connect to that gdbserver and debug your program. When you run
     * the 'monitor exit' command in gdb, gdbserver will terminate,
     * and so will linktty.
     */
    while (--argc > 0) {
	char *arg = *++argv;
        if (doing_opts && arg[0] == '-' && arg[1]) {
            if (!strcmp(arg, "--")) {
                doing_opts = false;
            } else if (!strcmp(arg, "--help")) {
                help(stdout);
                return 0;
            } else {
                errmsg("unrecognised argument '%s'", arg);
                return 1;
            }
        } else {
            if (nlinkfiles < 2) {
                ptys[nlinkfiles++].linkfile = arg;
            } else {
                command = argv;
                break;
            }
        }
    }

    for (i = 0; i < 2; i++) {
        char ptyname[FILENAME_MAX];
        struct termios attrs;
        struct pty *p = &ptys[i];

        p->master = pty_get(ptyname);
        p->slave = open(ptyname, O_RDWR);
        if (p->slave < 0) {
            perror("slave pty: open");
            exit(1);
        }

	tcgetattr(p->slave, &attrs);
	attrs.c_iflag &= ~(IXON | IXOFF | ICRNL | INLCR);
	attrs.c_oflag &= ~(ONLCR | OCRNL);
	attrs.c_lflag &= ~(ISIG | ICANON | ECHO);
	tcsetattr(p->slave, TCSADRAIN, &attrs);

        if (p->linkfile) {
            struct stat st;

            /* Clean up a pre-existing symlink at this location */
            if (lstat(p->linkfile, &st) == 0 &&
                ((st.st_mode & S_IFMT) == S_IFLNK))
                unlink(p->linkfile);

            if (symlink(ptyname, p->linkfile) < 0) {
                perror("symlink");
                while (i > 0)
                    unlink(ptys[--i].linkfile);
                exit(1);
            }
        } else {
            printf("%s\n", ptyname);
        }
    }

    for (i = 0; i < 2; i++)
        mainloop_add_forwarding(ptys[i].master, dup(ptys[i^1].master),
                                0.0, NULL, 0);

    pid_t childpid = -1;

    if (command) {
        mainloop_catch_signal(SIGCHLD);
        childpid = fork();
        if (childpid < 0) {
            perror("fork");
            exit(1);
        } else if (childpid == 0) {
            for (i = 0; i < 2; i++) {
                close(ptys[i].master);
                close(ptys[i].slave);
            }
	    execvp(command[0], command); /* assumes argv has trailing NULL */
            /*
             * If we're here, exec has gone badly foom.
             */
            perror("exec");
            exit(127);
        }
    }

    while (1) {
        int sig = mainloop_run();
        if (sig == SIGCHLD) {
            int pid, exitcode;
            pid = wait(&exitcode);
            if (childpid != -1 && childpid == pid ) {
                for (int i = 0; i < nlinkfiles; i++)
                    unlink(ptys[i].linkfile);
                return (WIFEXITED(exitcode) ? WEXITSTATUS(exitcode) :
                        128 | WTERMSIG(exitcode));
            }
        }
    }
}

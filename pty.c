/*
 * filter/pty.c - pseudo-terminal handling
 */

#include "cmake.h"

#if HAVE_FEATURES_H
#define _XOPEN_SOURCE 500
#include <features.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>

#include "filter.h"

/*
 * Allocate a pty. Returns a file handle to the master end of the
 * pty, and stores the full pathname to the slave end into `name'.
 * `name' is a buffer of size at least FILENAME_MAX.
 * 
 * Does not return on error.
 */
int pty_get(char *name)
{
    int fd;

    fd = open("/dev/ptmx", O_RDWR);

    if (fd < 0) {
	perror("/dev/ptmx: open");
	exit(1);
    }

    if (grantpt(fd) < 0) {
	perror("grantpt");
	exit(1);
    }
    
    if (unlockpt(fd) < 0) {
	perror("unlockpt");
	exit(1);
    }

    const char *sname = ptsname(fd);
    if (!sname) {
	perror("ptsname");
	exit(1);
    }

    name[FILENAME_MAX-1] = '\0';
    strncpy(name, sname, FILENAME_MAX-1);

    {
	struct termios ts;

	if (!tcgetattr(0, &ts))
	    tcsetattr(fd, TCSANOW, &ts);
    }

    pty_resize(fd);

    return fd;
}

/*
 * Pass the window size through from our own terminal to the inner
 * one.
 */
void pty_resize(int fd)
{
    struct winsize ws;

    if (!ioctl(0, TIOCGWINSZ, &ws))
        ioctl(fd, TIOCSWINSZ, &ws);
}

bool is_pty_master(int fd)
{
    return ptsname(fd) != 0;
}

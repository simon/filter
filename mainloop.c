#include <assert.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/mount.h>

#include "filter.h"

#define LOCALBUF_LIMIT 65536

#define min(x,y) ( (x)<(y)?(x):(y) )
#define max(x,y) ( (x)>(y)?(x):(y) )

/* ----------------------------------------------------------------------
 * Chunk of code lifted from PuTTY's misc.c to manage buffers of
 * data to be written to an fd.
 */

#define BUFFER_GRANULE  512

typedef struct bufchain_tag {
    struct bufchain_granule *head, *tail;
    int buffersize;		       /* current amount of buffered data */
} bufchain;
struct bufchain_granule {
    struct bufchain_granule *next;
    int buflen, bufpos;
    char buf[BUFFER_GRANULE];
};

static void bufchain_init(bufchain *ch)
{
    ch->head = ch->tail = NULL;
    ch->buffersize = 0;
}

static int bufchain_size(bufchain *ch)
{
    return ch->buffersize;
}

static void bufchain_add(bufchain *ch, const void *data, int len)
{
    const char *buf = (const char *)data;

    if (len == 0) return;

    ch->buffersize += len;

    if (ch->tail && ch->tail->buflen < BUFFER_GRANULE) {
	int copylen = min(len, BUFFER_GRANULE - ch->tail->buflen);
	memcpy(ch->tail->buf + ch->tail->buflen, buf, copylen);
	buf += copylen;
	len -= copylen;
	ch->tail->buflen += copylen;
    }
    while (len > 0) {
	int grainlen = min(len, BUFFER_GRANULE);
	struct bufchain_granule *newbuf;
	newbuf = (struct bufchain_granule *)
	    malloc(sizeof(struct bufchain_granule));
	if (!newbuf) {
	    errmsg("out of memory");
	    exit(1);
	}
	newbuf->bufpos = 0;
	newbuf->buflen = grainlen;
	memcpy(newbuf->buf, buf, grainlen);
	buf += grainlen;
	len -= grainlen;
	if (ch->tail)
	    ch->tail->next = newbuf;
	else
	    ch->head = ch->tail = newbuf;
	newbuf->next = NULL;
	ch->tail = newbuf;
    }
}

static void bufchain_consume(bufchain *ch, int len)
{
    struct bufchain_granule *tmp;

    assert(ch->buffersize >= len);
    while (len > 0) {
	int remlen = len;
	assert(ch->head != NULL);
	if (remlen >= ch->head->buflen - ch->head->bufpos) {
	    remlen = ch->head->buflen - ch->head->bufpos;
	    tmp = ch->head;
	    ch->head = tmp->next;
	    free(tmp);
	    if (!ch->head)
		ch->tail = NULL;
	} else
	    ch->head->bufpos += remlen;
	ch->buffersize -= remlen;
	len -= remlen;
    }
}

static void bufchain_prefix(bufchain *ch, void **data, int *len)
{
    *len = ch->head->buflen - ch->head->bufpos;
    *data = ch->head->buf + ch->head->bufpos;
}

/* ----------------------------------------------------------------------
 * End of bufchain stuff.
 */

static struct forwarding {
    struct forwarding *next;

    int fd_in, fd_out;

    double wait;
    bool used_wait;

    tstate *state;
    int translate_direction;

    int flags;
    bool is_pty;

    bufchain data;
} *forwardings = NULL;

static struct signalpipe {
    struct signalpipe *next;
    int fds[2];
    int sig;
} *signals = NULL;

static void write_char_to_signal_pipe(int fd, char ch)
{
    if (write(fd, &ch, 1) < 0) {
        /*
         * Ignore errors returned from write() on purpose (and signal
         * that to the compiler with this empty if statement, to
         * suppress gcc's warning).
         *
         * This is sensible because when we're writing to the write
         * end of an intra-process signal pipe, the only error we
         * expect is EAGAIN if the pipe buffer is already full, in
         * which case our aim of having the next iteration of the main
         * select loop handle the signal is already achieved.
         *
         * (OK, perhaps also some kind of EINVAL or EBADF or something
         * if a signal somehow arises during process shutdown, but
         * there's nothing we'd want to do in response to that
         * either.)
         */
    }
}

static void pipe_signal_handler(int sig)
{
    struct signalpipe *sp;
    for (sp = signals; sp; sp = sp->next)
        if (sp->sig == sig) {
            write_char_to_signal_pipe(sp->fds[1], 'x');
            return;
        }
}

void mainloop_catch_signal(int sig)
{
    struct signalpipe *sp = malloc(sizeof(struct signalpipe));
    if (!sp) {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    sp->next = signals;
    signals = sp;

    if (pipe(sp->fds) < 0) {
	perror("pipe");
	exit(1);
    }

    sp->sig = sig;
    signal(sig, pipe_signal_handler);
}

void mainloop_add_forwarding(int from, int to, double wait,
                             tstate *state, int translate_direction)
{
    struct forwarding *fw = malloc(sizeof(struct forwarding));
    if (!fw) {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    fw->next = forwardings;
    forwardings = fw;

    fw->fd_in = from;
    fw->fd_out = to;

    fw->wait = wait;
    fw->used_wait = false;
    fw->state = state;
    fw->translate_direction = translate_direction;

    fw->is_pty = is_pty_master(fw->fd_in);

    bufchain_init(&fw->data);
}

int mainloop_run(void)
{
    fd_set rset, wset;
    char buf[65536];
    int maxfd, ret;
    double twait;
    struct timeval tv, *ptv;
    struct signalpipe *sp;
    struct forwarding *fw;
    int toret;

    FD_ZERO(&rset);
    FD_ZERO(&wset);
    maxfd = 0;
    twait = HUGE_VAL;

    for (sp = signals; sp; sp = sp->next) {
        FD_SET(sp->fds[0], &rset);
        maxfd = max(sp->fds[0]+1, maxfd);
    }

    for (fw = forwardings; fw; fw = fw->next) {
        fw->flags = 0;
        fw->used_wait = false;

        if (fw->fd_out >= 0 && bufchain_size(&fw->data)) {
            FD_SET(fw->fd_out, &wset);
            maxfd = max(fw->fd_out+1, maxfd);
        }
        if (fw->fd_in >= 0 && bufchain_size(&fw->data) < LOCALBUF_LIMIT) {
            FD_SET(fw->fd_in, &rset);
            maxfd = max(fw->fd_in+1, maxfd);
            if (fw->wait) {
                fw->used_wait = true;
                twait = min(twait, fw->wait);
            }
        }
    }

    if (twait == HUGE_VAL) {
        ptv = NULL;
    } else {
        ptv = &tv;
        tv.tv_sec = (int)twait;
        tv.tv_usec = (int)1000000 * (twait - tv.tv_sec);
    }

    do {
        ret = select(maxfd, &rset, &wset, NULL, ptv);
    } while (ret < 0 && (errno == EINTR || errno == EAGAIN));

    if (ret == 0) {
        double left = (ret == 0 ? 0.0 : tv.tv_sec + tv.tv_usec/1000000.0);
        twait -= left;

        for (fw = forwardings; fw; fw = fw->next) {
            if (fw->wait && fw->used_wait) {
                fw->wait -= twait;
                assert(fw->wait >= 0);
                if (!fw->wait)
                    fw->flags |= EV_TIMEOUT;
            }
        }
    }

    if (ret < 0) {
        perror("select");
        return 1;
    }

    for (fw = forwardings; fw; fw = fw->next) {
        if (fw->fd_in >= 0 && FD_ISSET(fw->fd_in, &rset) || fw->flags) {
            char *translated;
            int translen;

            if (FD_ISSET(fw->fd_in, &rset)) {
                ret = read(fw->fd_in, buf, sizeof(buf));
                if (ret <= 0) {
                    /*
                     * EIO from a pty master just means end of
                     * file, annoyingly. Why can't it report
                     * ordinary EOF?
                     */
                    if (fw->is_pty && errno == EIO)
                        ret = 0;
                    if (ret < 0) {
                        perror("read");
                    }
                    close(fw->fd_in);
                    fw->fd_in = -1;
                    ret = 0;
                    fw->flags |= EV_EOF;
                }
            } else
                ret = 0;

            if (ret || fw->flags) {
                if (fw->state) {
                    translated = translate(fw->state, buf, ret, &translen,
                                           &fw->wait,
                                           fw->translate_direction, fw->flags);

                    if (translen > 0)
                        bufchain_add(&fw->data, translated, translen);

                    free(translated);
                } else {
                    bufchain_add(&fw->data, buf, ret);
                }
            }
        }

        if (fw->fd_out >= 0 && FD_ISSET(fw->fd_out, &wset)) {
            void *data;
            int len, ret;
            bufchain_prefix(&fw->data, &data, &len);
            if ((ret = write(fw->fd_out, data, len)) < 0) {
                perror("write");
                close(fw->fd_out);
                close(fw->fd_in);
                fw->fd_in = fw->fd_out = -1;
            } else
                bufchain_consume(&fw->data, ret);
        }

        /*
         * If there can be no further data from a direction (the
         * input fd has been closed and the buffered data is used
         * up) but its output fd is still open, close it.
         */
        if (fw->fd_in < 0 && !bufchain_size(&fw->data) && fw->fd_out >= 0) {
            /* In case the fd is a socket, try to SHUT_WR it to send EOF. */
            if (shutdown(fw->fd_out, SHUT_WR) < 0 && errno != ENOTSOCK)
                perror("shutdown(SHUT_WR)");

            close(fw->fd_out);
            fw->fd_out = -1;
        }
    }

    toret = 0;

    for (sp = signals; sp; sp = sp->next) {
        if (FD_ISSET(sp->fds[0], &rset)) {
            ret = read(sp->fds[0], buf, sizeof(buf));
            if (ret > 0) {
                toret = sp->sig;
                break;         /* leave other signal pipes for next run */
            }
        }
    }

    return toret;
}

static bool forwarding_active(struct forwarding *fw)
{
    /*
     * The condition `there is data flowing in a direction'
     * unpacks as: there is at least potentially data left to be
     * written (i.e. EITHER the input side of that direction is
     * still open OR we have some buffered data left from before
     * it closed) AND we are able to write it (the output side is
     * still active).
     */
    return (fw->fd_in >= 0 || bufchain_size(&fw->data) > 0) && fw->fd_out >= 0;
}

int mainloop_forwardings_active(void)
{
    struct forwarding *fw;
    int count = 0;
    for (fw = forwardings; fw; fw = fw->next)
        if (forwarding_active(fw))
            count++;
    return count;
}

int mainloop_forwardings_inactive(void)
{
    struct forwarding *fw;
    int count = 0;
    for (fw = forwardings; fw; fw = fw->next)
        if (!forwarding_active(fw))
            count++;
    return count;
}

int mainloop_forwardings_pending_output(void)
{
    struct forwarding *fw;
    int count = 0;
    for (fw = forwardings; fw; fw = fw->next)
        if (bufchain_size(&fw->data) > 0 && fw->fd_out >= 0)
            count++;
    return count;
}

bool mainloop_forwarding_active(int from, int to)
{
    for (struct forwarding *fw = forwardings; fw; fw = fw->next)
        if (fw->fd_in == from && fw->fd_out == to)
            return forwarding_active(fw);
    return false;
}

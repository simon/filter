/*
 * nulltrans.c - translation module for a filter which doesn't do
 * anything.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "filter.h"

const char *tstate_program_name = "nullfilter";
const char *tstate_program_purpose =
    "trivial pass-through filter that does nothing";
const char *tstate_arg_summary = NULL;
const char *const tstate_arg_help[] = { NULL };
const char *const tstate_option_help[] = { NULL };

tstate *tstate_init(void)
{
    return NULL;
}

int tstate_option(tstate *state, int shortopt, char *longopt, char *value)
{
    return OPT_UNKNOWN;
}

void tstate_argument(tstate *state, char *arg)
{
    errmsg("expected no arguments");
    exit(1);
}


void tstate_ready(tstate *state, double *idelay, double *odelay)
{
}

char *translate(tstate *state, char *data, int inlen, int *outlen,
		double *delay, int input, int flags)
{
    char *ret;

    ret = malloc(inlen);
    if (!ret) {
	errmsg("out of memory!");
	exit(1);
    }

    memcpy(ret, data, inlen);

    *outlen = inlen;
    *delay = 0.0;
    return ret;
}

void tstate_done(tstate *state)
{
    free(state);
}

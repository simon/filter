#ifndef FILTER_FILTER_H
#define FILTER_FILTER_H

#include <stdbool.h>

#define lenof(x) ( sizeof((x)) / sizeof(*(x)) )

typedef struct tstate tstate;

int pty_get(char *name);
void pty_resize(int fd);
bool is_pty_master(int fd);

void mainloop_add_forwarding(int from, int to, double wait,
                             tstate *state, int translate_direction);
void mainloop_catch_signal(int sig);
int mainloop_run(void);
int mainloop_forwardings_active(void);
int mainloop_forwardings_inactive(void);
int mainloop_forwardings_pending_output(void);
bool mainloop_forwarding_active(int from, int to);

void errmsg(const char *fmt, ...);

/* Return values from tstate_option */
#define OPT_UNKNOWN -1
#define OPT_SPURIOUSARG -2
#define OPT_MISSINGARG -3
#define OPT_OK 0

/* Special flags passed in to translate to indicate events. Always
 * accompanied with inlen==0. */
#define EV_TIMEOUT 1
#define EV_EOF 2

tstate *tstate_init(void);
int tstate_option(tstate *state, int shortopt, char *longopt, char *value);
void tstate_argument(tstate *state, char *arg);
void tstate_ready(tstate *state, double *idelay, double *odelay);
char *translate(tstate *state, char *data, int inlen, int *outlen,
		double *delay, int input, int flags);
void tstate_done(tstate *state);

/* For use in --help */
extern const char *
    tstate_program_name; /* the name of the utility */
extern const char *
    tstate_arg_summary;  /* one-line summary of positional args */
extern const char *
    tstate_program_purpose; /* one-line summary of what it does */
extern const char *const
    tstate_arg_help[]; /* longer help for positional args */
extern const char *const
    tstate_option_help[]; /* longer help for options */

#endif

/*
 * noprogress.c - translation module for an output filter which
 * removes any text followed by a CR without LF.
 *
 * The idea is that you use this to wrap the kind of program that
 * constantly reprints a progress bar of some kind (such as the build
 * tool 'ninja', or scp), and pipe its output to a pager, which can
 * then display the _sensible_ output lines without also including the
 * huge pile of overprinted progress indicators.
 *
 * It isn't very sophisticated. It only triggers on the use of CR to
 * overwrite, so anything that does its progress bar via backspaces
 * instead won't be caught. And it certainly can't untangle the
 * serious terminal-escape-heavy line editing done by readline.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "filter.h"

#include "charset.h"

const char *tstate_program_name = "noprogress";
const char *tstate_program_purpose =
    "suppress ephemeral progress reports overprinted with \\r";
const char *tstate_arg_summary = NULL;
const char *const tstate_arg_help[] = { NULL };
const char *const tstate_option_help[] = { NULL };

struct buffer {
    char *data;
    size_t len, size;
};

static void buffer_init(struct buffer *buf)
{
    buf->data = NULL;
    buf->len = buf->size = 0;
}

static void buffer_clear(struct buffer *buf)
{
    free(buf->data);
}

static void buffer_add(struct buffer *buf, const char *data, size_t len)
{
    if (buf->size - buf->len < len) {
        buf->size = (buf->len + len) * 5 / 4 + 512;
        buf->data = realloc(buf->data, buf->size);
        if (!buf->data) {
            errmsg("out of memory!");
            exit(1);
        }
    }

    memcpy(buf->data + buf->len, data, len);
    buf->len += len;
}

struct tstate {
    struct buffer linebuf;
    bool after_cr;
};

tstate *tstate_init(void)
{
    tstate *state;

    state = (tstate *)malloc(sizeof(tstate));
    if (!state) {
	errmsg("out of memory!");
	exit(1);
    }

    buffer_init(&state->linebuf);
    state->after_cr = false;

    return state;
}

int tstate_option(tstate *state, int shortopt, char *longopt, char *value)
{
    return OPT_UNKNOWN;
}

void tstate_argument(tstate *state, char *arg)
{
    errmsg("expected no arguments");
    exit(1);
}

void tstate_ready(tstate *state, double *idelay, double *odelay)
{
}

char *translate(tstate *state, char *data, int inlen, int *outlen,
		double *delay, int input, int flags)
{
    struct buffer outbuf;
    buffer_init(&outbuf);

    if (input) {
        /*
         * Input isn't interesting; just pass it through unchanged.
         */
        buffer_add(&outbuf, data, inlen);
    } else {
        while (inlen > 0) {
            if (*data != '\r' && *data != '\n') {
                /* Handle one or more other characters */

                if (state->after_cr) {
                    /* A non-\n after a \r: throw away the buffered line */
                    state->linebuf.len = 0;
                    state->after_cr = false;
                }

                /* Whether we did that or not, buffer the next lot of data */
                char *start = data;
                while (inlen > 0 && *data != '\r' && *data != '\n') {
                    data++;
                    inlen--;
                }
                buffer_add(&state->linebuf, start, data - start);
            } else {
                /* Treat \n and \r individually */
                if (*data == '\n') {
                    buffer_add(&outbuf, state->linebuf.data,
                               state->linebuf.len);
                    buffer_add(&outbuf, data, 1); /* emit the \n */
                    state->linebuf.len = 0;
                    state->after_cr = false;
                } else {
                    state->after_cr = true;
                    buffer_add(&state->linebuf, data, 1); /* cache the \r */
                }
                data++;
                inlen--;
            }
        }
    }

    *outlen = outbuf.len;
    *delay = 0.0;
    return outbuf.data;
}

void tstate_done(tstate *state)
{
    free(state);
}

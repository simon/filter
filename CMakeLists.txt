cmake_minimum_required(VERSION 3.5)
project(filter LANGUAGES C)

include(CheckIncludeFile)
check_include_file(features.h HAVE_FEATURES_H)

# Linuxoids like setpgrp(); BSDoids prefer setpgrp(0,0)
include(CheckCSourceCompiles)
check_c_source_compiles("
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char **argv) {
    setpgrp();
}" HAVE_NULLARY_SETPGRP)

set(GENERATED_SOURCES_DIR ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY})
include_directories(${GENERATED_SOURCES_DIR})
configure_file(cmake.h.in ${GENERATED_SOURCES_DIR}/cmake.h)

add_subdirectory(charset)
include_directories(charset)

add_library(filter-common STATIC
  main.c pty.c mainloop.c)

set(installed_programs)

function(add_filter NAME)
  cmake_parse_arguments(OPT "" "" "LIBRARIES" "${ARGN}")
  add_executable(${NAME} ${OPT_UNPARSED_ARGUMENTS})
  target_link_libraries(${NAME} ${OPT_LIBRARIES} filter-common)
  set(installed_programs ${installed_programs} ${NAME} PARENT_SCOPE)
endfunction()

add_filter(nullfilter nulltrans.c)
add_filter(csfilter cstrans.c LIBRARIES charset)
add_filter(asciify asciify.c LIBRARIES charset)
add_filter(nhfilter nhtrans.c)
add_filter(record record.c)
add_filter(idlewrapper idletrans.c)
add_filter(deidle deidle.c)
add_filter(noinput noinput.c)
add_filter(parity parity.c)
add_filter(noprogress noprogress.c)

add_executable(linktty linktty.c pty.c mainloop.c)
set(installed_programs ${installed_programs} linktty)

# Installation
include(GNUInstallDirs)
if(CMAKE_VERSION VERSION_LESS 3.14)
  # CMake 3.13 and earlier required an explicit install destination.
  install(TARGETS ${installed_programs} RUNTIME DESTINATION bin)
else()
  # 3.14 and above selects a sensible default, which we should avoid
  # overriding here so that end users can override it using
  # CMAKE_INSTALL_BINDIR.
  install(TARGETS ${installed_programs})
endif()

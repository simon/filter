/*
 * parity.c - cope with weird situations in which a server is sending
 * stuff to the terminal with some parity scheme still in effect.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "filter.h"

const char *tstate_program_name = "parity";
const char *tstate_program_purpose =
    "wrap a 7-bit program which uses the high bit for parity";
const char *tstate_arg_summary = NULL;
const char *const tstate_arg_help[] = { NULL };
const char *const tstate_option_help[] = {
    "--mark              always set parity bit on input",
    "--space             always clear parity bit on input",
    "--odd               ensure an odd number of bits set on input",
    "--even              ensure an even number of bits set on input",
    NULL
};

typedef enum { NONE, SPACE, EVEN } ParityType;

struct tstate {
    ParityType type;
    int flip;
};

tstate *tstate_init(void)
{
    tstate *state;

    state = (tstate *)malloc(sizeof(tstate));
    if (!state) {
	errmsg("out of memory!");
	exit(1);
    }

    state->type = NONE;

    return state;
}

int tstate_option(tstate *state, int shortopt, char *longopt, char *value)
{
    if (!strcmp(longopt, "mark")) {
        if (value)
            return OPT_SPURIOUSARG;
        state->type = SPACE;
        state->flip = 0x80;
        return OPT_OK;
    }
    if (!strcmp(longopt, "space")) {
        if (value)
            return OPT_SPURIOUSARG;
        state->type = SPACE;
        state->flip = 0;
        return OPT_OK;
    }
    if (!strcmp(longopt, "odd")) {
        if (value)
            return OPT_SPURIOUSARG;
        state->type = EVEN;
        state->flip = 0x80;
        return OPT_OK;
    }
    if (!strcmp(longopt, "even")) {
        if (value)
            return OPT_SPURIOUSARG;
        state->type = EVEN;
        state->flip = 0;
        return OPT_OK;
    }
    return OPT_UNKNOWN;
}

void tstate_argument(tstate *state, char *arg)
{
    errmsg("expected no arguments");
    exit(1);
}


void tstate_ready(tstate *state, double *idelay, double *odelay)
{
}

char *translate(tstate *state, char *data, int inlen, int *outlen,
		double *delay, int input, int flags)
{
    char *ret;
    int i;

    ret = malloc(inlen);
    if (!ret) {
	errmsg("out of memory!");
	exit(1);
    }

    if (input) {
        if (state->type == NONE) {
            memcpy(ret, data, inlen);
        } else if (state->type == EVEN) {
            for (i = 0; i < inlen; i++) {
                int c = data[i];
                int p = c;
                p ^= p << 4;
                p ^= p << 2;
                p ^= p << 1;
                c ^= p & 0x80;
                ret[i] = c ^ state->flip;
            }
        } else if (state->type == SPACE) {
            for (i = 0; i < inlen; i++) {
                int c = data[i];
                c &= 0x7F;
                ret[i] = c ^ state->flip;
            }
        }
    } else {
        for (i = 0; i < inlen; i++)
            ret[i] = data[i] & 0x7F;
    }

    *outlen = inlen;
    *delay = 0.0;
    return ret;
}

void tstate_done(tstate *state)
{
    free(state);
}

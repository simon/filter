/*
 * filter/main.c - main program.
 */

#include <assert.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ioctl.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/mount.h>

#include "cmake.h"
#include "filter.h"

struct termios oldattrs, newattrs;

void attrsonexit(void)
{
    tcsetattr(0, TCSANOW, &oldattrs);
}

typedef enum { KIND_PTY, KIND_PIPE, KIND_SOCKET } Kind;

struct mainopts {
    Kind kind;
    bool no_ctty;
    bool quit;
};

void usage(FILE *fp)
{
    static const char *const general_option_help[] = {
        "-p, --pipe          run subprocess in pipes instead of a pty",
        "-s, --socket        run subprocess in a socket instead of a pty",
        "-P, -S              like -p/-s, but also unset controlling terminal",
        "-q, --quit          quit on first potential reason instead of last",
        NULL
    };

    fprintf(fp, "usage:   %s [options]%s%s [-- subcommand [args]]\n",
            tstate_program_name, tstate_arg_summary ? " " : "",
            tstate_arg_summary ? tstate_arg_summary : "");
    fprintf(fp, "purpose: %s\n", tstate_program_purpose);
    const char *prefix = "args:    ";
    for (const char *const *p = tstate_arg_help; *p; p++) {
        fprintf(fp, "%s%s\n", prefix, *p);
        prefix = "         ";
    }
    prefix = "options: ";
    for (const char *const *p = tstate_option_help; *p; p++) {
        fprintf(fp, "%s%s\n", prefix, *p);
        prefix = "         ";
    }
    for (const char *const *p = general_option_help; *p; p++) {
        fprintf(fp, "%s%s\n", prefix, *p);
        prefix = "         ";
    }
    size_t namelen = strlen(tstate_program_name);
    fprintf(fp, "also:    %s --help%*s  display this text\n",
            tstate_program_name,
            (int)(namelen > 11 ? 0 : 11 - namelen), "");
}

int option(struct mainopts *opts, struct tstate *state,
	   int shortopt, char *longopt, char *value)
{
    /*
     * Process standard options across all filter programs here;
     * hand everything else on to tstate_option.
     */
    if (longopt) {
	if (!strcmp(longopt, "pipe")) {
	    shortopt = 'p';
	} else if (!strcmp(longopt, "socket")) {
	    shortopt = 's';
	} else if (!strcmp(longopt, "quit")) {
	    shortopt = 'q';
	} else if (!strcmp(longopt, "help")) {
            usage(stdout);
            exit(0);
	} else
	    return tstate_option(state, shortopt, longopt, value);
    }

    if (shortopt == 'p') {
	if (value)
	    return OPT_SPURIOUSARG;
	opts->kind = KIND_PIPE;
    } else if (shortopt == 'P') {
	if (value)
	    return OPT_SPURIOUSARG;
	opts->kind = KIND_PIPE;
        opts->no_ctty = true;
    } else if (shortopt == 's') {
	if (value)
	    return OPT_SPURIOUSARG;
	opts->kind = KIND_SOCKET;
    } else if (shortopt == 'S') {
	if (value)
	    return OPT_SPURIOUSARG;
	opts->kind = KIND_SOCKET;
        opts->no_ctty = true;
    } else if (shortopt == 'q') {
	if (value)
	    return OPT_SPURIOUSARG;
	opts->quit = true;
    } else
	return tstate_option(state, shortopt, longopt, value);

    return OPT_OK;
}

void errmsg(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "%s: ", tstate_program_name);
    vfprintf(stderr, fmt, ap);
    fputc('\n', stderr);
    va_end(ap);
}

int main(int argc, char **argv)
{
    int errors = 0;
    int masterr, masterw, slaver, slavew, pid;
    double iwait = 0.0, owait = 0.0;
    char ptyname[FILENAME_MAX];
    tstate *state;
    struct mainopts opts;
    int exitcode = -1;
    pid_t childpid = -1;

    opts.kind = KIND_PTY;
    opts.no_ctty = false;
    opts.quit = false;

    state = tstate_init();

    /*
     * Process command line.
     */
    while (--argc > 0) {
	char *p = *++argv;
	if (*p == '-') {
	    if (!strcmp(p, "--")) {
		--argc, ++argv;	       /* point at argument after "--" */
		break;		       /* finish processing options */
	    } else if (p[1] == '-') {
		char *q;
		int ret;

		q = strchr(p, '=');
		if (q)
		    *q++ = '\0';

		ret = option(&opts, state, '\0', p+2, q);
		if (ret == OPT_MISSINGARG) {
		    assert(!q);

		    if (--argc > 0) {
			q = *++argv;
			ret = option(&opts, state, '\0', p+2, q);
		    } else {
			fprintf(stderr, "option %s expects an argument\n", p);
			errors = 1;
		    }
		}
		
		if (ret == OPT_UNKNOWN) {
		    fprintf(stderr, "unknown long option: %s\n", p);
		    errors = 1;
		} else if (ret == OPT_SPURIOUSARG) {
		    fprintf(stderr, "option %s does not expect an argument\n",
			    p);
		    errors = 1;
		}
	    } else {
		char c;
		int ret;

		while ((c = *++p) != '\0') {
		    ret = option(&opts, state, c, NULL, NULL);
		    if (ret == OPT_MISSINGARG) {
			char *q;

			if (p[1])
			    q = p+1;
			else if (--argc > 0)
			    q = *++argv;
			else {
			    fprintf(stderr, "option -%c expects an"
				    " argument\n", c);
			    errors = 1;
			    q = NULL;
			}
			if (q)
			    ret = option(&opts, state, c, NULL, q);
		    }

		    if (ret == OPT_UNKNOWN) {
			fprintf(stderr, "unknown option: -%c\n", c);
			errors = 1;
		    } else if (ret == OPT_SPURIOUSARG) {
			fprintf(stderr, "option -%c does not expect an"
				" argument\n", c);
			errors = 1;
		    }
		}
	    }
	} else {
	    tstate_argument(state, p);
	}
    }
    if (errors)
	return 1;

    mainloop_catch_signal(SIGCHLD);

    /*
     * Allocate the pty or pipes.
     */
    if (opts.kind == KIND_PIPE) {
	int p[2];
	if (pipe(p) < 0) {
	    perror("pipe");
	    return 1;
	}
	masterr = p[0];
	slavew = p[1];
	if (pipe(p) < 0) {
	    perror("pipe");
	    return 1;
	}
	slaver = p[0];
	masterw = p[1];
    } else if (opts.kind == KIND_SOCKET) {
        int s[2];
        if (socketpair(PF_UNIX, SOCK_STREAM, 0, s) < 0) {
	    perror("socketpair");
	    return 1;
	}
	masterr = s[0];
        masterw = dup(masterr);
	slaver = s[1];
        slavew = dup(slaver);
    } else {
        mainloop_catch_signal(SIGWINCH);

	masterr = pty_get(ptyname);
	masterw = dup(masterr);
	slaver = open(ptyname, O_RDWR);
	slavew = dup(slaver);
	if (slaver < 0) {
	    perror("slave pty: open");
	    return 1;
	}
    }

    tstate_ready(state, &iwait, &owait);

    /*
     * Fork and execute the command.
     */
    pid = fork();
    if (pid < 0) {
	perror("fork");
	return 1;
    }

    if (pid == 0) {
	int i;
	/*
	 * We are the child.
	 */
	close(masterr);
	close(masterw);

	fcntl(slaver, F_SETFD, 0);    /* don't close on exec */
	fcntl(slavew, F_SETFD, 0);    /* don't close on exec */
	close(0);
	dup2(slaver, 0);
	close(1);
	dup2(slavew, 1);
	if (opts.kind == KIND_PTY || opts.no_ctty) {
	    int fd;
	    close(2);
	    dup2(slavew, 2);
	    setsid();
#if HAVE_NULLARY_SETPGRP
            setpgrp();
#else
            setpgrp(0, 0);
#endif
	    tcsetpgrp(0, getpgrp());
	    if ((fd = open("/dev/tty", O_RDWR)) >= 0) {
		ioctl(fd, TIOCNOTTY);
		close(fd);
	    }
            if (opts.kind == KIND_PTY)
                ioctl(slavew, TIOCSCTTY);
	}
	/* Close everything _else_, for tidiness. */
	for (i = 3; i < 1024; i++)
	    close(i);
	if (argc > 0) {
	    execvp(argv[0], argv);     /* assumes argv has trailing NULL */
	} else {
	    execl(getenv("SHELL"), getenv("SHELL"), NULL);
	}
	/*
	 * If we're here, exec has gone badly foom.
	 */
	perror("exec");
	exit(127);
    }

    /*
     * Now we're the parent. Close the slave fds and start copying
     * stuff back and forth.
     */
    close(slaver);
    close(slavew);
    childpid = pid;

    if (opts.kind == KIND_PTY) {
	tcgetattr(0, &oldattrs);
	newattrs = oldattrs;
	newattrs.c_iflag &= ~(IXON | IXOFF | ICRNL | INLCR);
	newattrs.c_oflag &= ~(ONLCR | OCRNL);
	newattrs.c_lflag &= ~(ISIG | ICANON | ECHO);
	atexit(attrsonexit);
	tcsetattr(0, TCSADRAIN, &newattrs);
    }

    mainloop_add_forwarding(0, masterw, iwait, state, 1);
    mainloop_add_forwarding(masterr, 1, owait, state, 0);

    while (1) {
        int retd = mainloop_run();

        if (retd == SIGCHLD) {
            int pid, code;
            pid = wait(&code);     /* reap the exit code */
            if (pid == childpid)
                exitcode = code;
        } else if (retd == SIGWINCH) {
            pty_resize(masterr);
        }

        /*
         * Termination condition with pipes or sockets is that there's
         * still data flowing in at least one direction.
         *
         * Termination condition for a pty-based run is that the
         * child process hasn't yet terminated and/or there is
         * still buffered data to send.
         */
        if (opts.kind != KIND_PTY) {
            if (opts.quit) {
                /*
                 * For pipe-based runs, -q causes us to quit on EOF in
                 * the outward direction (that is, when the child
                 * process closes its stdout and we receive EOF on the
                 * pipe we're reading). Not the other way round,
                 * because too many pipe-type programs will still want
                 * to give important output after they see EOF on
                 * input. (e.g. 'sort' and similar).
                 */
                if (exitcode >= 0 ||
                    !mainloop_forwarding_active(masterr, 1))
                    break;
            } else {
                if (mainloop_forwardings_active() == 0)
                    break;
            }
        } else {
            if (opts.quit) {
                /*
                 * For pty-based runs, -q causes us to quit when any
                 * forwarding dies, because with a pty, there's no
                 * really separate notion of inward vs outward EOF.
                 */
                if (exitcode >= 0 ||
                    mainloop_forwardings_inactive() > 0)
                    break;
            } else {
                if (exitcode >= 0 &&
                    mainloop_forwardings_pending_output() == 0)
                    break;
            }
        }
    }

    close(masterw);
    close(masterr);

    if (exitcode < 0) {
	int pid, code;
	pid = wait(&code);
	exitcode = code;
    }

    tstate_done(state);

    return (WIFEXITED(exitcode) ? WEXITSTATUS(exitcode) :
	    128 | WTERMSIG(exitcode));
}
